package com.iostreams.services;

import com.iostreams.wrapper.FileWrapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.io.*;

public class IOStreamsServiceTest {

    private static final String FILE_WRITE_PATH = "D:\\Java DevEducation\\iostreams\\src\\main\\resources\\FileForWrite";
    private static final String FILE_READ_PATH = "D:\\Java DevEducation\\iostreams\\src\\main\\resources\\";

    FileWrapper fileWrapper = Mockito.mock(FileWrapper.class);
    File file = Mockito.mock(File.class);
    FileInputStream fis = Mockito.mock(FileInputStream.class);
    FileOutputStream fos = Mockito.mock(FileOutputStream.class);
    FileWriter fw = Mockito.mock(FileWriter.class);
    FileReader fr = Mockito.mock(FileReader.class);

    IOStreamsService cut = new IOStreamsService();

    @Test
    void overwriteFileTest() {

        try {

            for (int i = 1; i < Byte.MAX_VALUE; i++) {

                Mockito.when(fileWrapper.getFile(i)).thenReturn(file);
                Mockito.when(fileWrapper.getFileForReading(file)).thenReturn(fis);

            }

            Mockito.when(fileWrapper.getFileForWriting()).thenReturn(fos);

            cut.overwriteFile();

            //Mockito.verify(file, Mockito.times(Byte.MAX_VALUE)).exists();
            //Mockito.verify(fis, Mockito.times(Byte.MAX_VALUE)).read();
            //Mockito.verify(dataFromFile, Mockito.times(Byte.MAX_VALUE)).add(1);
            //Mockito.verify(fis, Mockito.times(Byte.MAX_VALUE)).close();
            //Mockito.verify(fos, Mockito.times(1)).write(dataForFile);
            //Mockito.verify(fos, Mockito.times(1)).close();

        } catch (IOException ex) {

            ex.printStackTrace();

        }
    }

//    @Test
//    void overwriteFileExceptionTest() {
//
//        try {
//
//            for (int i = 1; i < Byte.MAX_VALUE; i++) {
//
//                Mockito.when(fileWrapper.getFile(i)).thenReturn(file);
//                Mockito.when(fileWrapper.getFileForReading(file)).thenReturn(fis);
//
//            }
//
//            Mockito.when(fileWrapper.getFileForWriting()).thenReturn(fos);
//
//            Exception ex = new IOException();
//            File testFile = new File(":fdsf");
//
//            Mockito.doThrow(ex).when(fileWrapper).getFileForReading(testFile);
//            Mockito.verify(ex, Mockito.times(1)).printStackTrace();
//
//        } catch (IOException ex) {
//
//            ex.printStackTrace();
//
//        }
//    }

    @Test
    void appendToBeginningOfFileTest() {

        try {

            for (int i = 1; i < Byte.MAX_VALUE; i++) {

                Mockito.when(fileWrapper.getFile(i)).thenReturn(file);
                Mockito.when(fileWrapper.getFileForReading(file)).thenReturn(fis);

            }

            Mockito.when(fileWrapper.getFileInputStream()).thenReturn(fis);
            Mockito.when(fileWrapper.getFileForWriting()).thenReturn(fos);

            cut.appendToBeginningOfFile();

        } catch (IOException ex) {

            ex.printStackTrace();

        }
    }

    @Test
    void appendToEndOfFileTest() {

        try {

            Mockito.when(fileWrapper.getFileWriter(true)).thenReturn(fw);

            for (int i = 1; i < Byte.MAX_VALUE; i++) {

                Mockito.when(fileWrapper.getFileReader(i)).thenReturn(fr);

            }

            cut.appendToEndOfFile();

        } catch (IOException ex) {

            ex.printStackTrace();

        }
    }

}
