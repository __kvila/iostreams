package com.iostreams.wrapper;

import java.io.*;

public class FileWrapper {

    private static final String FILE_WRITE_PATH = "D:\\Java DevEducation\\iostreams\\src\\main\\resources\\FileForWrite";
    private static final String FILE_READ_PATH = "D:\\Java DevEducation\\iostreams\\src\\main\\resources\\";

    public File getFile(int number) {

        return new File(FILE_READ_PATH + number);

    }

    public FileInputStream getFileForReading(File file) throws IOException {

        return new FileInputStream(file);

    }

    public FileOutputStream getFileForWriting() throws IOException {

        return new FileOutputStream(FILE_WRITE_PATH);

    }

    public FileWriter getFileWriter(boolean append) throws IOException {

        return new FileWriter(FILE_WRITE_PATH, append);

    }

    public FileReader getFileReader(int number) throws IOException {

        return new FileReader(FILE_READ_PATH + number);

    }

    public FileInputStream getFileInputStream () throws IOException {

        return new FileInputStream(FILE_WRITE_PATH);

    }

}
