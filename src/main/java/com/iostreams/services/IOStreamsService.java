package com.iostreams.services;

import com.iostreams.wrapper.FileWrapper;
import java.io.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class IOStreamsService {

    private static final Logger LOG_CONSOLE = Logger.getLogger("loggerConsole");
    private static final Logger LOG_FILE = Logger.getLogger("loggerFile");

    FileWrapper fw = new FileWrapper();

    public void overwriteFile() {

        ArrayList<Integer> dataFromFile = new ArrayList<>();
        byte [] dataForFile;

        try {

            for (int i = 1; i < Byte.MAX_VALUE; i++) {

                File file = fw.getFile(i);

                if (file.exists()) {

                    FileInputStream fis = fw.getFileForReading(file);

                    int symbol;
                    while ((symbol = fis.read()) != -1) {

                        dataFromFile.add(symbol);

                    }

                    fis.close();

                }
            }

            dataForFile = new byte[dataFromFile.size()];

            int i = 0;
            for (int a : dataFromFile) {

                dataForFile[i] = (byte) a;

                i++;

            }

            FileOutputStream fos = fw.getFileForWriting();
            fos.write(dataForFile);
            fos.close();

        } catch (IOException ex) {

            LOG_CONSOLE.error(ex.getMessage());
            LOG_FILE.error(ex.getMessage());

        }
    }

    public void appendToBeginningOfFile() {

        ArrayList<Integer> dataFromFile = new ArrayList<>();
        byte [] dataForFile;

        try  {

            for (int i = 1; i < Byte.MAX_VALUE; i++) {

                File file = fw.getFile(i);

                if (file.exists()) {

                    FileInputStream fis = fw.getFileForReading(file);

                    int symbol;
                    while ((symbol = fis.read()) != -1) {

                        dataFromFile.add(symbol);

                    }

                    fis.close();

                }

            }

            FileInputStream fis = fw.getFileInputStream();
            int symbol;
            while ((symbol = fis.read()) != -1) {

                dataFromFile.add(symbol);

            }

            fis.close();

            dataForFile = new byte[dataFromFile.size()];

            int i = 0;
            for (int a : dataFromFile) {

                dataForFile[i] = (byte) a;

                i++;

            }

            FileOutputStream fos = fw.getFileForWriting();
            fos.write(dataForFile);
            fos.close();


        } catch (IOException ex) {

            LOG_CONSOLE.error(ex.getMessage() + ex.getMessage());
            LOG_FILE.error(ex.getMessage() + ex.getMessage());

        }
    }

    public void appendToEndOfFile() {

        char [] dataFromFile;

        try (FileWriter fileWriter = fw.getFileWriter(true)) {

            for (int i = 1; i <= Byte.MAX_VALUE; i++) {

                File file = fw.getFile(i);
                int fsize = (int) file.length();

                if (file.exists()) {

                    FileReader fr = fw.getFileReader(i);
                    dataFromFile = new char[fsize];
                    fr.read(dataFromFile);
                    fileWriter.write(dataFromFile);

                    fr.close();

                }
            }
        } catch (IOException ex) {

            LOG_CONSOLE.error(ex.getMessage() + ex.getMessage());
            LOG_FILE.error(ex.getMessage() + ex.getMessage());

        }
    }
}
